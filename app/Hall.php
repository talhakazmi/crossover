<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function stands(){
        return $this->hasMany('App\Stand');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stand extends Model
{
    public function hall(){
        return $this->belongsTo('App\Hall');
    }
}

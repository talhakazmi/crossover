import routes from './app.routes';

export default ($stateProvider, $urlRouterProvider, localStorageServiceProvider) => {
    'ngInject';

    routes($stateProvider, $urlRouterProvider);
};
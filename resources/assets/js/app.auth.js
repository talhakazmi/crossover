class UserAuth {

    constructor(localStorageService){
        'ngInject';
        this.localStorageService = localStorageService;
    }
    setUser(user){
        this.localStorageService.set('userid',user.id);
        this.localStorageService.set('name',user.name);
        this.localStorageService.set('email',user.email);

    }

    getUserId(){
        return this.localStorageService.get('userid');
    }

    getUserName(){
        return this.localStorageService.get('name');
    }

    getUserEmail(){
        return this.localStorageService.get('email');
    }
    resetStorage(){
        this.localStorageService.clearAll();
    }
}

export default UserAuth;
import 'jquery';
import "angular";
import 'angular-ui-router';
import 'angular-local-storage'
import '../sass/app.scss';
import UserAuth from './app.auth'

import config from './app.config';

angular
    .module('crossover', [
        'ui.router',
        'LocalStorageModule'
    ])
    //loading config from other file
    .config(config)
    .factory('UserAuth', UserAuth)
    //creating component
    .component('myHeader', {
        template: require('./app/components/header/header.html'),
        controller: require('./app/components/header/header.ctrl').default,
        controllerAs: '$ctrl'
    })
    .component('gmap', {
        //html used for templating gmap component
        template: require('./app/components/gmap/gmap.html'),
        //default controller for gmap component
        controller: require('./app/components/gmap/gmap.ctrl').default,
        //bind custom methods and properties with gmap component
        bindings: {
            onClick: '&',
            onMarkerClick: '&',
            width: '@',
            height: '@',
        }
    })
    .component('hallGrid', {
        template: require('./app/components/hall-grid/hall-grid.html'),
        controller: require('./app/components/hall-grid/hall-grid.ctrl').default,
        bindings: {
            onCellClick: '&',
            showForm: '@',
            rows: '@',
            cols: '@',
            activeCell: '=',
        }
    });

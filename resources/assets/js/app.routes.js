export default ($stateProvider, $urlRouterProvider) => {
    $stateProvider
        /*
         description: login route where user can login to the system
         */
        .state('home', {
            url: '/home',
            template: require('./app/modules/home/home.html'),
            controller: require('./app/modules/home/home.ctrl.js').default,
            controllerAs: '$ctrl'
        })
        .state('signin', {
            url: '/signin',
            template: require('./app/modules/signin/signin.html'),
            controller: require('./app/modules/signin/signin.ctrl.js').default,
            controllerAs: '$ctrl'
        })
        .state('signup', {
            url: '/signup',
            template: require('./app/modules/signup/signup.html'),
            controller: require('./app/modules/signup/signup.ctrl.js').default,
            controllerAs: '$ctrl'
        })
    /*
     default route to user map.
     */
    $urlRouterProvider.otherwise('/home');
};
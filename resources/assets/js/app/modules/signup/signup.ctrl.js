class SignupCtrl {
    constructor ($state, $http, UserAuth){
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.UserAuth = UserAuth;
        this.data = {
            name: '',
            email: '',
            password: '',
            password_confirmation: ''
        };
        this.error = {
          name: '',
          email: '',
          password: '',
        };
    }

    submit(){
        this.$http.post('http://localhost/single-page/public/register',this.data)
            .then(response => {
                if(response.data.id > 0)
                {
                    this.UserAuth.setUser(response.data);
                    this.$state.go('home');
                }
            })
            .catch(error => {
                this.error = error.data;
            })
    }
}

export default SignupCtrl;
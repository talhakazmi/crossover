class SigninCtrl {
    constructor ($state,$http,UserAuth){
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.UserAuth = UserAuth;
        this.data = {
            email: '',
            password: ''
        }
        this.error = {
            email: '',
            password: ''
        };
    }

    submit(){
        this.$http.post('http://localhost/single-page/public/login',this.data)
            .then(response => {
                if(response.data.id > 0)
                {
                    this.UserAuth.setUser(response.data);
                    this.$state.go('home');
                }
            })
            .catch(error => {
                this.error = error.data;
            })
    }
}

export default SigninCtrl;
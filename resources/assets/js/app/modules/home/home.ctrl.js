class HomeCtrl{

    constructor($state, $http, UserAuth){
        'ngInject';
        this.UserAuth = UserAuth;
        this.onMapClick = this.onMapClick.bind(this);
        this.data = {location:''};
    }

    onFormSubmit(data) {
        this.$http.post('api/event', data)
            .then(response => {
                this.$state.go('adminHall', { eventId:  response.data.id});
            })
            .catch((error) => {
                console.log(error);
            });
    }

    onMapClick(map, event) {
        const $content = angular.element(require('./info-window.html'));

        $content.find('.submit').click(() => {
            this.onFormSubmit({
                title: $content.find('.title').val(),
                startdate: $content.find('.startDate').val(),
                enddate: $content.find('.endDate').val(),
                latitude: event.latLng.lat(),
                longitude: event.latLng.lng(),
                location: this.data.location,
            });
            infoWindow.close();
        });

        $($content.find('.date-picker')).datepicker({
            autoHide:true,
            format:'yyyy-mm-dd'
        });

        const infoWindow = new google.maps.InfoWindow({
            content: $content[0],
            maxWidth: 500,
        });
        const marker = new google.maps.Marker({
            position: event.latLng,
            map,
        });
        const geocoder = new google.maps.Geocoder;

        geocoder.geocode({'location': {lat:event.latLng.lat(), lng: event.latLng.lng()}},  (results, status) => {
            if (status === 'OK') {
                this.data.location = results[1].formatted_address;
            }
        });

        infoWindow.open(map, marker);
    }

}

export default HomeCtrl;
class HeaderCtrl {

    constructor($state, $http, UserAuth){
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.userid = UserAuth.getUserId();
        this.name = UserAuth.getUserName();
        this.UserAuth = UserAuth;
    }
    signin(){
        this.$state.go('signin');
    }

    signup(){
        this.$state.go('signup');
    }

    home(){
        this.$state.go('home');
    }
    signout(){

        this.$http.post('http://localhost/single-page/public/logout',{'X-CSRF-TOKEN':document.getElementsByName('csrf-token')[0].content})
            .then(response => {
                if(response.data.status == 'success')
                {
                    this.UserAuth.resetStorage();
                    this.$state.go('home');
                }
            })
            .catch(error => {
                this.error = error.data;
            })
    }
}

export default HeaderCtrl;
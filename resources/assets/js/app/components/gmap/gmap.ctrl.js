class GmapCtrl {
    constructor($http, $timeout, $element) {
        'ngInject';

        this.$http = $http;
        this.$timeout = $timeout;
        this.map = new google.maps.Map($element.find('.gmap')[0], {
            zoom: 4,
            center: {lat: -25.363, lng: 131.044}
        });
    }

    $onInit() {
        this.onClick = this.onClick();
        this.onMarkerClick = this.onMarkerClick();
        if (this.onClick) {
            google.maps.event.addListener(this.map, 'click', event => {
                this.onClick(this.map, event);
                this.$timeout();
            });
        }
        this.$http.get('event')
            .then(response => {
                console.log(response);
                for (let i = 0; i < response.data.length; i++) {
                    const $content = angular.element(require('./info-window.html'));
                    let myLatlng = new google.maps.LatLng(response.data[i].latitude, response.data[i].longitude);
                    const marker = new google.maps.Marker({
                        position: myLatlng,
                        map: this.map,
                    });

                    $content.find('.title').html(response.data[i].event);
                    $content.find('.location').html(response.data[i].location);
                    $content.find('.date').html(response.data[i].start_date + ' to ' + response.data[i].end_date);
                    const infoWindow = new google.maps.InfoWindow({
                        content: $content[0],
                        maxWidth: 500,
                    });

                    marker.addListener('click', () => {
                        infoWindow.open(this.map, marker);
                        if(this.onMarkerClick)
                        {
                            this.onMarkerClick(response.data[i]);
                            this.$timeout();
                        }
                    });
                }
            })
            .catch(error => {
                console.log(error);
            });
    }
}

export default GmapCtrl;
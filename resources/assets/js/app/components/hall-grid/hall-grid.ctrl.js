import './styles.scss';

class HallGridCtrl {
    constructor() {
        'ngInject';
        this.grid = [];
    }

    $onInit() {
        this.onCellClick = this.onCellClick();

        const available = {};

        if (this.activeCell && this.activeCell.length) {
            this.activeCell.forEach(cell => {
                available[`${cell.x}-${cell.y}`] = cell;
            });
        }
        const rows = parseInt(this.rows);
        const cols = parseInt(this.cols);

        this.rowHeight = 100 / rows;
        this.cellWidth = 100 / cols;

        if (rows && cols) {
            for (let x = 0; x < rows; x++) {
                const cells = [];
                for (let y = 0; y < cols; y++) {
                    cells.push(
                        available[`${x}-${y}`]
                            ? Object.assign({
                                    active: true,
                                }, available[`${x}-${y}`]
                            )
                            : {
                                x,
                                y,
                            }
                    );
                }
                this.grid.push(cells);
            }
        }
    }
}

export default HallGridCtrl;